const fs = require('fs');

function generarArchivo(archivo,texto){
    fs.appendFile(texto, archivo + "\n", function(err){
        if(err) return console.log(err);
        console.log(`${archivo}, cargado.`);
    })
}

exports.generarArchivo = generarArchivo;