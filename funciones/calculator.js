function sumar (v1, v2){
    if(!isNaN(v1) && !isNaN(v2)){
        return `${v1} + ${v2} = ${v1 + v2}`;
    }else{
        return `Uno de los valores no era un numero.`;
    }
}

function restar (v1,v2){
    if(!isNaN(v1) && !isNaN(v2)){
        return `${v1} - ${v2} = ${v1 - v2}`;
    }else{
        return `Uno de los valores no era un numero.`;

    }
}

function multiplicar (v1,v2){
    if(!isNaN(v1) && !isNaN(v2)){
        return `${v1} * ${v2} = ${v1 * v2}`;
    }else{
        return `Uno de los valores no era un numero.`;
    }
}

function dividir(v1,v2){
    if(!isNaN(v1) && !isNaN(v2)){
        if(v2 == 0){
        return `No es posible dividir por 0`;
    }else{
        return `${v1} / ${v2} = ${v1 / v2}`;
    }
}else{

   return `Uno de los valores no era un numero`

}

}


exports.sumar = sumar;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;